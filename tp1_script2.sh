#!/bin/bash
# Mattéo Ferreira
# 19/10/2020
# Verrouillage et Extinction du pc
sec=$2
clear
echo Countdown :
    while [ $sec -ge 0 ]; do
        echo -ne "$sec\033[0K\r"
            let "sec=sec-1"
                sleep 1
                    done
clear
#Execution
if [ "$1" == "shutdown" ]; then
#Shutdown
    sudo shutdown -h now     
#Lock
elif [ "$1" == "lock" ]; then
    /System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend

fi

