## HostOS

Nom de la machine, OS et version
```
MacBook-Air-de-KW:~ Matteo$ system_profiler SPSoftwareDataType 

$ [...]
System Version: macOS 10.13.6 (17G14019)
Computer Name: MacBook Air de KW
[...]
```
Processeur
```
MacBook-Air-de-KW:~ Matteo$ system_profiler -detailLevel full
[...]
IOAccelMemoryInfo:
[...]
64-Bit (Intel): Yes
```
RAM
```
MacBook-Air-de-KW:~ Matteo$ system_profiler SPHardwareDataType
[...]
Memory: 8 GB
[...]

MacBook-Air-de-KW:~ Matteo$ system_profiler SPMemoryDataType
Memory:

    Memory Slots:

      ECC: Disabled
      Upgradeable Memory: No

        BANK 0/DIMM0:

          Size: 4 GB
          Type: DDR3
          Speed: 1600 MHz
          Status: OK
          Manufacturer: 0x80AD
          Part Number: 0x483943434E4E4E424C54424C41522D4E5444
          Serial Number: -

        BANK 1/DIMM0:

          Size: 4 GB
          Type: DDR3
          Speed: 1600 MHz
          Status: OK
          Manufacturer: 0x80AD
          Part Number: 0x483943434E4E4E424C54424C41522D4E5444
          Serial Number: -
```
## Devices



Marque du processeur
```
MacBook-Air-de-KW:~ Matteo$ system_profiler | grep 
[...]
Processor Name: Intel Core i5
Number of Processors: 1
[...]
```
Nombre de coeurs:
```
MacBook-Air-de-KW:~ Matteo$ system_profiler SPHardwareDataType
[...]
Total Number of Cores: 2  
[...]
```
Modèle du processeur
```
MacBook-Air-de-KW:~ Matteo$ sysctl -n machdep.cpu.brand_string
Intel(R) Core(TM) i5-5350U CPU @ 1.80GHz
```
explication de i5-5350U:
i5 -> Chez intel, cela signifie un milieu de gamme.
5 -> La génération de la puce, donc ici la 5ème génération
350 -> positionnement dans la famille + révision éventuelle.
U -> basse consommation (15 W / les meilleurs amis des ultraportables)

Marque et modèle du TouchPad : Pas trouvé

Marque et modèle de la carte graphique : 
```
MacBook-Air-de-KW:~ Matteo$ system_profiler SPDisplaysDataType

[...]
Intel HD Graphics 6000
Device ID: 0x1626
Revision ID: 0x0009
[...]
```
Disques Dures
```
MacBook-Air-de-KW:~ Matteo$ diskutil list
/dev/disk0 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *121.3 GB   disk0
   1:                        EFI EFI                     209.7 MB   disk0s1
   2:                 Apple_APFS Container disk1         121.1 GB   disk0s2

/dev/disk1 (synthesized):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      APFS Container Scheme -                      +121.1 GB   disk1
                                 Physical Store disk0s2
   1:                APFS Volume Macintosh HD            95.2 GB    disk1s1
   2:                APFS Volume Preboot                 44.6 MB    disk1s2
   3:                APFS Volume Recovery                1.0 GB     disk1s3
   4:                APFS Volume VM                      2.1 GB     disk1s4
```
Il y a deux disques, un disque physique (le disk0) et un autre virtuel (le disk1). 

Sur le disque 0, il y a 2 partitions.
Sur le disque 1, il y a 4 partitions.

la partition EFI du disque principale (0) represente un fichier .efi qui est nécéssaire au démarrage de l'OS.

Preboot disk1s2 est le volume de prédémarrage nécéssaire à l'EFI
Recovery disk1s3 est le volume qui contient l'OS de secours
VM disk1s4 (Virtual Memory : il contient le fichier "sleepimage" qui archive le contexte de la RAM).


## Users

Liste complete des utilisateurs 
```
Air-de-KW:~ Matteo$ dscl . list /Users
_amavisd
_analyticsd
_appleevents
_applepay
_appowner
_appserver
_appstore
_ard
_assetcache
[...]
_webauthserver
_windowserver
_www
_wwwproxy
_xserverdocs
daemon
Guest
kwbayonne
Matteo
nobody
root
```
L'utilisateur qui à le droit de tout faire sur la machine est l'utilisateur **root**


## Processus

Liste des processus
```
Air-de-KW:~ Matteo$ ps aux
USER               PID  %CPU %MEM      VSZ    RSS   TT  STAT STARTED      TIME COMMAND
Matteo             409   2,4  0,6  4590732  51276   ??  S     2:11     0:43.65 /Applications/Utilities/Terminal.app/Contents/MacOS/Terminal
_windowserver      159   1,5  0,5  5547228  44468   ??  Ss    2:10     7:35.71 /System/Library/PrivateFrameworks/SkyLight.framework/Resources/WindowServer -daemon
_hidd               98   0,9  0,0  4335200   3992   ??  Ss    2:10     1:13.08 /usr/libexec/hidd
Matteo             488   0,7  3,3 17204064 280000   ??  S     2:11     4:38.28 /Applications/Discord.app/Contents/Frameworks/Discord Helper (Renderer).app/Contents/MacOS/Discord Helper (Renderer) --typ
Matteo             380   0,3  0,0  4332068   2524   ??  S     2:11     0:01.08 /System/Library/PrivateFrameworks/ViewBridge.framework/Versions/A/XPCServices/ViewBridgeAuxiliary.xpc/Contents/MacOS/ViewB
_mysql              91   0,3  0,1  4867268  10684   ??  Ss    2:10     0:40.80 /usr/local/mysql/bin/mysqld --user=_mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --plugin-dir=/usr/loc
Matteo             532   0,1  0,3  4616196  23776   ??  S     2:12     0:22.44 /Applications/Brave Browser.app/Contents/Frameworks/Brave Browser Framework.framework/Versions/86.1.15.72/Helpers/Brave Br
root              8748   0,0  0,1  4304432   5448 s000  Ss    2:24     0:00.03 login -pf Matteo
root              8603   0,0  0,0  4331404   2448   ??  Ss    1:00     0:00.15 /usr/libexec/powerlogd
[...]
root                64   0,0  0,0  4336312   1348   ??  SNs  Sam02     0:01.17 /usr/libexec/warmd
root               101   0,0  0,0  4362972   1300   ??  Ss   Sam02     0:03.92 /usr/libexec/amfid
root                54   0,1  0,0  4331356   2108   ??  Ss    2:10     0:04.62 /System/Library/CoreServices/powerd.bundle/powerd
root                44   0,0  0,1  4340536   7748   ??  Ss    2:10     0:05.03 /usr/libexec/kextd
root                41   0,0  0,1  4333164   5776   ??  Ss    2:10     0:02.19 /usr/libexec/UserEventAgent (System)
root                40   0,0  0,0  4305532    772   ??  Ss    2:10     0:02.50 /usr/sbin/syslogd
root              2932   0,0  0,0  4268124   1024 s000  R+   11:11     0:00.01 ps aux
root                 1   0,0  0,1  4326984  10268   ??  Ss    2:10     0:24.99 /sbin/launchd
Matteo            2923   0,0  0,2  4332804  14936   ??  S    11:11     0:00.14 /System/Library/PrivateFrameworks/SyncedDefaults.framework/Support/syncdefaultsd
root              2876   0,0  0,3  4305168  25388   ??  Ss   11:07     0:00.06 /System/Library/CoreServices/backupd.bundle/Contents/Resources/backupd
Matteo            2846   0,0  0,5  8819968  38384   ??  S    11:05     0:00.11 /Applications/Brave Browser.app/Contents/Frameworks/Brave Browser Framework.framework/Versions/86.1.15.72/Helpers/Brave Br
root              2820   0,0  0,3  4340332  23012   ??  Ss   11:03     0:07.80 /System/Library/CoreServices/ReportCrash daemon
```

5 services systèmes:

* ```root                     /sbin/launchd``` -> Pendant le démarrage du système, launchd est le premier processus exécuté par le noyau pour configurer l’ordinateur (d'où le `1` en PID)

* ```root /usr/libexec/powerlogd``` -> Cette commande gère des archives de journaux structurées qui permettent la récupération des données de puissance et de performances du système.

* ```root /usr/libexec/kextd``` -> kextd est le serveur d'extension du noyau. Il est autonome pour gérer les requêtes du noyau et d'autres processus de l'espace utilisateur.

* ```root /usr/libexec/amfid``` -> amfid est un démon qui vérifie l'intégrité des fichiers exécutés sur le système.

* ```root /usr/libexec/warmd``` -> warmd contrôle les caches utilisés lors du démarrage et de la connexion 


## Network

Liste des cartes réseaux 

```
Air-de-KW:~ Matteo$ networksetup -listallhardwareports

Hardware Port: Wi-Fi
Device: en0
Ethernet Address: d4:61:9d:39:aa:8e

Hardware Port: Bluetooth PAN
Device: en2
Ethernet Address: d4:61:9d:39:aa:8f

Hardware Port: Thunderbolt 1
Device: en1
Ethernet Address: 9a:00:0f:a2:6d:f0

Hardware Port: Thunderbolt Bridge
Device: bridge0
Ethernet Address: 9a:00:0f:a2:6d:f0

VLAN Configurations
===================
```
* Wi-Fi : il s'agit de la carte permettant la connexion Wi-fi, en0 signifie l'interface de l'appareil, ici AirPort(Wifi), enfin "Ethernet Adress" est l'adresse MAC du port.

* Bluetooth PAN : il s'agit de la carte permettant la connexion Bluetooth. Avec pour interface en2, signifiant Ethernet. 

* Tunderbolt 1 : il s'agit du port Thunderbolt, avec comme interface en1, siginifiant Ethernet.

* Thunderbolt Bridge : il s'agit d'un pont entre le port Thunderbolt physique et l'interface réseau virtuel, d'où l'interface bridge0.

liste des Port TCP et UDP
```

Air-de-KW:~ Matteo$ netstat -a
Active Internet connections (including servers)
Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)    
tcp4       0      0  air-de-kw.56113        ec2-13-235-40-15.http  ESTABLISHED
tcp4       0      0  air-de-kw.56111        ec2-54-251-12-15.http  ESTABLISHED
tcp4       0      0  air-de-kw.56109        customer.worldst.http  ESTABLISHED
tcp4       0      0  air-de-kw.56108        172.67.144.164.http    ESTABLISHED
tcp4       0      0  air-de-kw.56092        server-13-225-25.https ESTABLISHED
tcp4       0      0  air-de-kw.56082        server-13-35-43-.https ESTABLISHED
tcp4       0      0  air-de-kw.56077        a2-22-77-162.dep.https ESTABLISHED
tcp4       0      0  air-de-kw.56076        a2-22-77-162.dep.https ESTABLISHED
tcp4       0      0  air-de-kw.56065        yandex.ru.https        ESTABLISHED
tcp4       0      0  air-de-kw.56061        104.16.29.34.https     ESTABLISHED
tcp4       0      0  air-de-kw.56060        192.0.73.2.https       ESTABLISHED
tcp4       0      0  air-de-kw.56059        151.101.193.69.https   ESTABLISHED
tcp4       0      0  air-de-kw.56035        a23-38-120-97.de.https ESTABLISHED
tcp4       0      0  air-de-kw.56034        104.16.79.80.https     ESTABLISHED
tcp4       0      0  air-de-kw.56033        151.101.122.217.https  ESTABLISHED
tcp4       0      0  air-de-kw.56021        151.101.121.7.https    ESTABLISHED
tcp4       0      0  air-de-kw.56010        151.101.120.84.https   ESTABLISHED
tcp4      31      0  air-de-kw.55889        151.101.121.7.https    CLOSE_WAIT 
tcp4      31      0  air-de-kw.55417        151.101.121.7.https    CLOSE_WAIT 
tcp4       0      0  air-de-kw.55362        ec2-18-182-129-6.https ESTABLISHED
tcp4       0      0  air-de-kw.55039        172.67.184.82.https    ESTABLISHED
tcp4       0      0  air-de-kw.55035        104.31.141.11.https    ESTABLISHED
tcp4       0      0  air-de-kw.55032        104.31.140.11.https    ESTABLISHED
tcp4       0      0  air-de-kw.55027        162.159.130.234.https  ESTABLISHED
tcp4       0      0  air-de-kw.55024        17.57.146.36.5223      ESTABLISHED
tcp4       0      0  localhost.ipp          *.*                    LISTEN     
tcp6       0      0  localhost.ipp          *.*                    LISTEN     
tcp6       0      0  *.53874                *.*                    LISTEN     
tcp4       0      0  *.53874                *.*                    LISTEN     
tcp4       0      0  localhost.6463         *.*                    LISTEN     
tcp46      0      0  *.mysql                *.*                    LISTEN     
tcp46      0      0  *.33060                *.*                    LISTEN     
tcp4       0      0  air-de-kw.56081        wordpress.com.https    TIME_WAIT  
tcp4       0      0  air-de-kw.56079        192.0.78.25.https      TIME_WAIT  
tcp4       0      0  air-de-kw.56083        192.0.78.19.https      TIME_WAIT  
udp4       0      0  air-de-kw.56861        *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.mdns                 *.*                               
udp6       0      0  *.54284                *.*                               
udp4       0      0  *.54284                *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.53778                *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp46      0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp6       0      0  *.mdns                 *.*                               
udp4       0      0  *.mdns                 *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.netbios-dgm          *.*                               
udp4       0      0  *.netbios-ns           *.* 
[...]
```

Le port TCP4 est la connexion internet vers des sites internet, donc ici "Local Address" est l'adresse de mon Mac et "Foreign Address" est l'adresse de destination comme par exemple les sites internets.
TCP = Lent et stable 
UDP = Rapide et instable

## II.Scripting


## III.Gestion de softs

* Un gestionnaire de paquets, c’est un outil qui permet d’installer des logiciels, de les désinstaller et de les mettre à jour.
Tous les logiciels (les paquets) sont centralisés dans un seul et même serveur (le dépôt).
Les gestionnaires de paquets permettent de mettre à disposition simplement des milliers de paquetages lors d'une installation standard.

* l'Identités des personnes sont protégés car les paquets sont stockés sur des serveurs tiers.

* Tous les logiciels sont dépourvus de spywares et malwares en tout genre : toutes les cases qui installaient des logiciels non désirés n’existent plus.


**Lister les paquets installés**

```
MacBook-Air-de-KW:~ Matteo$ brew list
gettext		heroku		mysql		pcre2
git		heroku-node	oniguruma	protobuf
go		jq		openssl@1.1
```

Homebrew installe les paquets dans leurs propres répertoires et crée des liens symboliques de leurs fichiers vers /usr/local.

## IV. Machine Virtuelle

### Configuration post-install

Apres avoir créé la Machine Virtuelle, on peut lister les cartes réseaux avec `ip a` 

![](https://i.imgur.com/2IMvHsg.png)

On peut voir que la carte `enp0s8` n'est pas encore activé, il faut donc l'activer avec la commande `ifup enp0s8`

![](https://i.imgur.com/fq6JgK9.png)

On peut refaire la commande `ip a` pour optenir l'adresse IP de la carte `enp0s8`

![](https://i.imgur.com/wF4jXmc.png)


Donc l'adresse IP est : `192.168.57.3`

On peut donc creer une connexion SSH : 

``` 
Air-de-KW:~ Matteo$ ssh root@192.168.57.3
root@192.168.57.3's password: 
Last login: Fri Nov 13 10:26:33 2020
[root@localhost ~]# 
```

### IV. Partage de fichiers

* Pour monter un partage NFS on peut donc commencer par modifier le fichier `/etc/exports` Pour y renseigner l'adresse IP de notre VM et son masque : 
 On effectue la commande `sudo nano /etc/exports`
 On peut ainsi modifier le fichier en y renseignant le dossier qu'on veux partage, l'adresse réseau que l'on veux partager et son masque : 

![](https://i.imgur.com/bBDCPSX.png)


* Ensuite pour activer le partage il ecrire la commande `sudo nfsd enable`


```
Air-de-KW:~ Matteo$ sudo nfsd enable
The nfsd service is already enabled.
```

* Pour vérifier si le partage est activé on peut faire la commande `showmount -e` : 

```
Air-de-KW:~ Matteo$ showmount -e
Exports list on localhost:
/Users/Matteo/Documents/Projects    192.168.120.0
```

Le partage NFS est donc activé du coté serveur (Ordinateur)

Il faut donc l'activer du coté Client (Machine Virtuelle)

Dans la Machine Virtuelle, il faut télécharger les ressources utiles pour NFS
il faut donc télécharger `yum install nfs-utils`

![](https://i.imgur.com/rXWTzoN.png)

(ici, ces ressources étaient déjà installés)

Créer un dossier où se fera le partage : 
ici `/mnt/tp1`

![](https://i.imgur.com/vaRlsYQ.png)


Ensuite il suffit d'utiliser la commande `mount -t nfs <ip_serveur>:<dossier_du_serveur> <dossier_du_client>`

![](https://i.imgur.com/51NyrsW.png)

J'ai recu une erreur que je n'ai pas su resoudre... 
Je n'ai donc pas pu conclure et partager les fichiers.





























